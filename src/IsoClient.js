var IsoClient = IsoClient || {};

(function () {
  IsoClient.parser = function (doc, config) {
    function parseValue(value, parsingOptions) {
      if ($(value).text()) {
        return parsingOptions.valueProps ? parse(value, parsingOptions.valueProps) : $(value).text();
      }
    }

    function parseWithXpath(xmlNode, xPath) {
      return $(xmlNode).xpath(xPath, function (ns) {
        if (ns === 'gmd') {
          return 'http://www.isotc211.org/2005/gmd';
        } else if (ns === 'gco') {
          return 'http://www.isotc211.org/2005/gco';
        } else if (ns === 'gmi') {
          return 'http://www.isotc211.org/2005/gmi';
        }
      });
    }

    function parseWithMultiValueXpath(xmlNode, xPath, parsingOptions) {
      var res = [];
      _.each(parseWithXpath(xmlNode, xPath), function (value) {
        res.push(parseValue(value, parsingOptions));
      });
      return res;
    }
    
    function parseProperty(xmlNode, parsingOptions) {
      var xPath = parsingOptions.xPath,
        multiValue = parsingOptions.multiValue;
      return multiValue ?
          parseWithMultiValueXpath(xmlNode, xPath, parsingOptions) :
          parseValue(parseWithXpath(xmlNode, xPath), parsingOptions);
    }

    function parse(xmlNode, configOptions) {
      var parsedResults = {};

      _.each(configOptions, function (parsingOptions, propertyName) {
        parsedResults[propertyName] = parseProperty(xmlNode, parsingOptions);
      });

      return parsedResults;
    }

    return parse(doc, config);
  };

  IsoClient.createParamsString = function (params) {
    var self = this;
    this.paramString = '';
    if (!_.isEmpty(params)) {
      this.paramString = '?';
    }
    _.each(params, function (value, key) {
      self.paramString = self.paramString + key + '=' + value + '&';
    }, self);

    this.paramString = this.paramString.substring(0, this.paramString.length - 1);

    return this.paramString;
  };

  IsoClient.fetch = function (baseUrl, options) {
    var paramString;
    options = options ? _.clone(options) : {};
    if (!options.success) {
      options.success = function () {};
    }
    if (!options.error) {
      options.error = function () {};
    }

    paramString = this.createParamsString(_.pick(options, 'state', 'major_version'));

    var isoConfig = {
      'title': {
        'xPath': './/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString'
      },
      'summary': {
        'xPath': './/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString'
      },
      'investigators': {
        'multiValue': true,
        'xPath': './/gmd:MD_DataIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty[.//gmd:CI_RoleCode="principalInvestigator"]' +
          '//gmd:individualName[not(contains(gco:CharacterString, "NSIDC User Services"))]'
      },
      'parameters': {
        'multiValue': true,
        'xPath': './/gmd:MD_Keywords[.//gmd:MD_KeywordTypeCode="discipline"]/gmd:keyword/gco:CharacterString'
      },
      'instruments': {
        'multiValue': true,
        'xPath': './/gmi:MI_Instrument/gmi:identifier/gmd:MD_Identifier/gmd:code/gco:CharacterString'
      },
      'documentationLink': {
        xPath: './/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:name/' +
                    'gco:CharacterString[text()=\'Documentation\']/../../..',
        'valueProps': {
          'url': {
            'xPath': './gmd:CI_OnlineResource/gmd:linkage/gmd:URL'
          },
          'displayText': {
            'xPath':  './gmd:CI_OnlineResource/gmd:name/gco:CharacterString'
          },
          'altText': {
            'xPath': './gmd:CI_OnlineResource/gmd:description/gco:CharacterString'
          },
          'function': {
            'xPath': './gmd:CI_OnlineResource/gmd:function/gmd:CI_OnLineFunctionCode'
          }
        }
      },
      'dataAccessLinks': {
        'multiValue': true,
        'xPath': './/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:name/' +
                    'gco:CharacterString[not(text()=\'Documentation\')]/../../..',
        'valueProps': {
          'url': {
            'xPath': './gmd:CI_OnlineResource/gmd:linkage/gmd:URL'
          },
          'displayText': {
            'xPath':  './gmd:CI_OnlineResource/gmd:name/gco:CharacterString'
          },
          'altText': {
            'xPath': './gmd:CI_OnlineResource/gmd:description/gco:CharacterString'
          },
          'function': {
            'xPath': './gmd:CI_OnlineResource/gmd:function/gmd:CI_OnLineFunctionCode'
          }
        }
      },
      'internalDataCenters': {
        'multiValue': true,
        'xPath': '//gmd:MD_DataIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty[.//gmd:CI_RoleCode="custodian"]',
        'valueProps': {
          'organizationShortName': {
            'xPath': './gmd:organisationShortName/gco:CharacterString'
          },
          'organizationName' : {
            'xPath' : './gmd:organisationName/gco:CharacterString'
          },
          'onlineResourceUrl' : {
            'xPath' : './gmd:contactInfo/gmd:CI_Contact/gmd:onlineResource/gmd:CI_OnlineResource/gmd:linkage/gmd:URL'
          }
        }
      }
    }, nsidcMetadataFormat;

    $.ajax({
      url: baseUrl + '.iso' + paramString,
      async: false
    }).done(function (document) {
      nsidcMetadataFormat = IsoClient.parser(document, isoConfig);

      $.ajax({
        dataType: 'json',
        url: baseUrl + '.json' + paramString,
        async: false
      }).done(function (data) {
        $.extend(nsidcMetadataFormat, data);

        $.ajax({
          dataType: 'json',
          url: baseUrl + '/citation' + paramString,
          async: false
        }).done(function (data) {
          $.extend(nsidcMetadataFormat, data);
          options.success(nsidcMetadataFormat);
        }).fail(function (jqXHR, textStatus, errorThrown) {
          options.error(jqXHR, textStatus, errorThrown);
          return;
        });
      }).fail(function (jqXHR, textStatus, errorThrown) {
        options.error(jqXHR, textStatus, errorThrown);
        return;
      });
    }).fail(function (jqXHR, textStatus, errorThrown) {
      options.error(jqXHR, textStatus, errorThrown);
      return;
    });

    return nsidcMetadataFormat;
  };
})();