describe("Fetch", function () {
  var should,
      server,
      nsidc_json,
      datasetUrlBase = "/data/TEST123",
      datasetIsoUrl = "/data/TEST123.iso",
      datasetCitationUrl = "/data/TEST123/citation",
      datasetNsidcJson = "/data/TEST123.json",
      citation = "{\"dataCitation\": \"Test Data Citation\"," +
                   "\"literatureCitations\": [\"Test Literature Citation\"]}",
      nsidcDatasetJson = "{\"title\":\"Test Title\",\"datasetState\":\"Published No QC\"," +
        "\"majorVersion\":1,\"newestPublishedMajorVersion\":2," +
        "\"seeAlsoResources\":[{\"uri\": \"http://nsidc.com/test.html\", \"displayText\": \"Test See Also\"}]}";

  before(function () {
    should = chai.should();
  });

  describe("Callbacks", function () {
    it("Should ahve the same value in the success callback and resturn value", function () {
      var nsidc_json_return, successCallback = sinon.stub(), errorCallback = sinon.stub();
      server = sinon.fakeServer.create();
      server.respondWith("GET", datasetIsoUrl, [200, { "Content-Type": "application/xml" }, isoDocument]);
      server.respondWith("GET", datasetCitationUrl, [200, { "Content-Type": "application/json" }, citation]);
      server.respondWith("GET", datasetNsidcJson, [200, { "Content-Type": "application/json" }, nsidcDatasetJson]);
      nsidc_json_return = IsoClient.fetch(datasetUrlBase, {success: successCallback, error: errorCallback});
      nsidc_json = successCallback.args[0][0];
      nsidc_json.should.eql(nsidc_json_return);
    });
  });

  describe("state and version service calls", function () {
    it("Should use the state parameter to call the service", function () {
      server = sinon.fakeServer.create();
      server.respondWith("GET", datasetIsoUrl + "?state=draft", [200, { "Content-Type": "application/xml" }, isoDocument]);
      server.respondWith("GET", datasetCitationUrl + "?state=draft", [200, { "Content-Type": "application/json" }, citation]);
      server.respondWith("GET", datasetNsidcJson + "?state=draft", [200, { "Content-Type": "application/json" }, nsidcDatasetJson]);
      nsidc_json = IsoClient.fetch(datasetUrlBase, {state: "draft"});
      should.exist(nsidc_json);
    });

    it("Should use the version parameter to call the service", function () {
      server = sinon.fakeServer.create();
      server.respondWith("GET", datasetIsoUrl + "?major_version=4", [200, { "Content-Type": "application/xml" }, isoDocument]);
      server.respondWith("GET", datasetCitationUrl + "?major_version=4", [200, { "Content-Type": "application/json" }, citation]);
      server.respondWith("GET", datasetNsidcJson + "?major_version=4", [200, { "Content-Type": "application/json" }, nsidcDatasetJson]);
      nsidc_json = IsoClient.fetch(datasetUrlBase, {major_version: 4});
      should.exist(nsidc_json);
    });

    it("Should use the state and version parameter to call the service", function () {
      server = sinon.fakeServer.create();
      server.respondWith("GET", datasetIsoUrl + "?state=draft&major_version=4",
        [200, { "Content-Type": "application/xml" }, isoDocument]);
      server.respondWith("GET", datasetCitationUrl + "?state=draft&major_version=4",
        [200, { "Content-Type": "application/json" }, citation]);
      server.respondWith("GET", datasetNsidcJson + "?state=draft&major_version=4",
        [200, { "Content-Type": "application/json" }, nsidcDatasetJson]);
      nsidc_json = IsoClient.fetch(datasetUrlBase, {state: "draft", major_version: 4});
      should.exist(nsidc_json);
    });
  });

  describe("Successful service call", function () {
    beforeEach(function () {
      server = sinon.fakeServer.create();
      server.respondWith("GET", datasetIsoUrl, [200, { "Content-Type": "application/xml" }, isoDocument]);
      server.respondWith("GET", datasetCitationUrl, [200, { "Content-Type": "application/json" }, citation]);
      server.respondWith("GET", datasetNsidcJson, [200, { "Content-Type": "application/json" }, nsidcDatasetJson]);
      nsidc_json = IsoClient.fetch(datasetUrlBase);
    });

    afterEach(function () {
      server.restore();
    });

    it("Should correctly set dataset title", function () {
      nsidc_json.title.should.eql("Test Title");
    });
  
    it("Should correctly set the summary", function () {
      nsidc_json.summary.should.eql("Test Summary");
    });
  
    it("Should correctly set the data_citation", function () {
      nsidc_json.dataCitation.should.eql("Test Data Citation");
    });
  
    it("Should correctly set the literature citation", function () {
      nsidc_json.literatureCitations[0].should.eql("Test Literature Citation");
    });
  
    it("should correclty set the guide documentation link", function () {
      nsidc_json.documentationLink.url.should.eql("http://nsidc.org/guide_doc_test.html");
      nsidc_json.documentationLink.function.should.eql("information");
      nsidc_json.documentationLink.displayText.should.eql("Documentation");
      nsidc_json.documentationLink.altText.should.eql("Documentation explaining the data and how it was processed.");
    });
  
    it("should correctly set the data_access link", function () {
      nsidc_json.dataAccessLinks[0].url.should.eql("ftp://sidads.colorado.edu/pub/DATASETS/fgdc/ggd221_soiltemp_antarctica/");
      nsidc_json.dataAccessLinks[0].displayText.should.eql("Get Data");
      nsidc_json.dataAccessLinks[0].function.should.eql("download");
      nsidc_json.dataAccessLinks[0].altText.should.eql("Data Access URL");
    });
  
    it("should correctly set the see also resource", function () {
      nsidc_json.seeAlsoResources[0].uri.should.eql("http://nsidc.com/test.html");
      nsidc_json.seeAlsoResources[0].displayText.should.eql("Test See Also");
    });
  
    it("should correctly set the investigators", function () {
      nsidc_json.investigators[0].should.eql("Claire L. Parkinson");
      nsidc_json.investigators[1].should.eql("Per Gloersen");
    });
  
    it("should correctly set the parameters", function () {
      nsidc_json.parameters[0].should.eql("EARTH SCIENCE > Cryosphere > Sea Ice > Sea Ice Concentration");
      nsidc_json.parameters[1].should.eql("EARTH SCIENCE > Oceans > Sea Ice > Sea Ice Concentration");
    });
  
    it("should correctly set the instruments", function () {
      nsidc_json.instruments[0].should.eql("SMMR > Scanning Multichannel Microwave Radiometer");
    });
  
    it("should correctly set the archiving data center", function () {
      nsidc_json.internalDataCenters[0].organizationName.should.eql("Making Earth System Data Records for Use in Research Environments");
      nsidc_json.internalDataCenters[0].organizationShortName.should.eql("NSIDC_MEASURES");
      nsidc_json.internalDataCenters[1].organizationName.should.eql("NASA DAAC at the National Snow and Ice Data Center");
      nsidc_json.internalDataCenters[1].organizationShortName.should.eql("NSIDC_DAAC");
    });
  
    it("should correctly set the archiving data center  URLs", function () {
      nsidc_json.internalDataCenters[0].onlineResourceUrl.should.eql("http://nsidc.org/data/measures/index.html");
      nsidc_json.internalDataCenters[1].onlineResourceUrl.should.eql("http://nsidc.org/daac/index.html");
    });
  });
  
  describe("Error service call", function () {
    beforeEach(function () {
      server = sinon.fakeServer.create();
    });

    afterEach(function () {
      server.restore();
    });

    it("calls the error callback on ISO error", function () {
      var successCallback = sinon.stub(), errorCallback = sinon.stub();

      server.respondWith("GET", datasetIsoUrl, [500, { "Content-Type": "text/html" }, ""]);

      nsidc_json = IsoClient.fetch(datasetUrlBase, {success: successCallback, error: errorCallback});
      errorCallback.should.have.been.called;
      successCallback.should.not.have.been.called;
    });
  });
});