describe("Parsing", function () {

  var config = {
    "title": {
      "xPath": "//title"
    },
    "authors": {
      "xPath": "//author",
      "multiValue": true
    },
    "dataUrl": {
      "xPath": "//data_url",
      "valueProps": {
        "url": {
          "xPath": "./url"
        },
        "name": {
          "xPath": "./name"
        }
      }
    },
    "investigators": {
      "xPath": "//contact",
      "multiValue": true,
      "valueProps": {
        "name": {
          "xPath": "./full_name"
        },
        "phoneNumber": {
          "xPath": "./phone_number"
        },
        "email": {
          "xPath": "./email"
        }
      }
    }
  };
  var doc = "<document>" +
              "<title>Testing Title</title>" +
              "<author>Jane Doe</author>" +
              "<author>John Doe</author>" +
              "<data_url>" +
                "<url>test.com</url>" +
                "<name>Test URL</name>" +
              "</data_url>" +
              "<contact>" +
                "<full_name>Jane Doe</full_name>" +
                "<phone_number>555-5555</phone_number>" +
                "<email>janedoe@test.com</email>" +
              "</contact>" +
              "<contact>" +
                "<full_name>John Doe</full_name>" +
                "<phone_number>555-6666</phone_number>" +
                "<email>johndoe@test.com</email>" +
              "</contact>" +
            "</document>";

  before(function () {
    chai.should();
  });

  it("Contructs an object that contains each configuration property", function () {
    var hash = IsoClient.parser(doc, config);

    _.keys(hash).should.eql(_.keys(config));
  });

  it("Assigns single xpath match to the corresponding property", function () {
    var hash = IsoClient.parser(doc, config);

    hash.title.should.eql("Testing Title");
  });

  it("Assigns multiple xpath matches to the corresponding property", function () {
    var hash = IsoClient.parser(doc, config);

    hash.authors.length.should.eql(2);
  });

  it("Creates an object with the given properties for the corresponding xpath", function () {
    var hash = IsoClient.parser(doc, config);

    _.keys(hash.dataUrl).should.eql(_.keys(config.dataUrl.valueProps));
  });

  it("Creates an object with the given properties for multiple xpath matches", function () {
    var hash = IsoClient.parser(doc, config);

    hash.investigators.length.should.eql(2);
    _.keys(hash.investigators[0]).should.eql(_.keys(config.investigators.valueProps));
  });
});
