/*global module */

module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jshint: {
      options: {
        jshintrc: '.jshintrc',

        // Custom Globals
        globals: {         // additional predefined global variables
          module : true
        }
      },

      with_defaults: ['Gruntfile.js', 'src/*.js'],

      with_testing_overrides: {
        options: {
          jshintrc: '.jshintrc_for_tests'
        },
        files: {
          src: ['spec/**/*.js']
        }
      }
    },

    mocha_phantomjs: {
      unit: ['spec/unit/*.html']
    },

    mocha_html: {
      unit: {
        html : 'spec/unit/unit.html',
        src   : ['lib/*.js', 'fixtures/*.js', 'src/*.js'],
        test  : [ 'spec/unit/*_spec.js' ],
        assert : 'chai',
        checkLeaks : false
      }
    },

    watch: {
      files: ['Gruntfile.js', 'spec/**/*.js', 'src/*.js'],
      tasks: ['lint', 'unit']
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-mocha-phantomjs');
  grunt.loadNpmTasks('grunt-mocha-html');

  grunt.registerTask('unit', ['mocha_html:unit', 'mocha_phantomjs:unit']);
  grunt.registerTask('lint', ['jshint']);
  grunt.registerTask('default', ['lint', 'unit']);
};